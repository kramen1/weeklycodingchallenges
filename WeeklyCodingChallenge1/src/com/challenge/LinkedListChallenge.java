package com.challenge;

import java.util.LinkedList;

public class LinkedListChallenge {

		public static void main(String[] args) {
			LinkedList<Integer> X = new LinkedList<>(); 
			LinkedList<Integer> Y = new LinkedList<>(); 
			X.add(3);
			X.add(7);
			X.add(8);
			X.add(10);
			Y.add(99); 
			Y.add(1); 
			Y.add(8); 
			Y.add(10); 
			
			for(int node : X) {
				if(Y.contains(node)) {
					System.out.println("Common value is: " + node);
					return; 
				}
			
		}
	}		

}
